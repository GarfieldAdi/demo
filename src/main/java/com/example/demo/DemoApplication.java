package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {
	private final Logger LOG = LoggerFactory.getLogger(DemoApplication.class);

	private final List<Book> books = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("/books")
	public List<Book> getAllBooks() {
		LOG.debug("Getting all books: {}", books);
		return books;
	}

	@GetMapping("/books/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable int id) {
		LOG.debug("Getting book by id: {}", id);
		for (Book book : books) {
			if (book.getId() == id) {
				return new ResponseEntity<>(book, HttpStatus.OK);
			}
		}
		LOG.debug("Book not found");
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("/books")
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		LOG.debug("Adding book: {}", book);
		books.add(book);
		return new ResponseEntity<>(book, HttpStatus.CREATED);
	}

	@PutMapping("/books/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable int id, @RequestBody Book book) {
		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).getId() == id) {
				books.set(i, book);
				return new ResponseEntity<>(book, HttpStatus.OK);
			}
		}
		LOG.debug("Book not found");
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/books/{id}")
	public ResponseEntity<Void> deleteBook(@PathVariable int id) {
		LOG.debug("Delete book by id {}", id);
		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).getId() == id) {
				books.remove(i);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}
		LOG.debug("Book not found");
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}

class Book {
	private int id;
	private String title;
	private String author;

	private String review;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Book {" +
				"id =  " + id +
				", title='" + title + '\'' +
				", author='" + author + '\'' +
				'}';
	}
}

