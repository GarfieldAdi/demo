FROM maven:3.6.3-jdk-11 AS build
WORKDIR /build
COPY . /build
RUN mvn clean install

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=build /build/target/*.jar /app/demo-SNAPSHOT.jar
EXPOSE 8002
ENTRYPOINT ["java", "-jar", "demo-SNAPSHOT.jar"]
